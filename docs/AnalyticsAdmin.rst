Administration Manual
=================================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Bbox (Bounding box)|Rectangle that restricts the image space with the detected object   |
|                   |                                                                    |
|                   |(vehicle or license plate).                                         |
+-------------------+--------------------------------------------------------------------+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Classifier         |A system parameter that recognizes one of the vehicle or license    |
|                   |                                                                    |
|                   |plate attributes.                                                   |
+-------------------+--------------------------------------------------------------------+
|Detection          |Actions to determine areas of the image that contain vehicle or LP. |
+-------------------+--------------------------------------------------------------------+
|Event              |Detected vehicle with extracted attributes on a specific source.    |
+-------------------+--------------------------------------------------------------------+
|Incident           |Event founded in lists.                                             |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
----------------

This document is manual for administrator of the service for displaying the results of CARS API and CARS Stream work through the CARS Analytics 2.0.9 web interface.

This manual describes the installation, configuration and administration of the service.

It is recommended to carefully read this manual before installing and using the service.

General information
------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs CARS Analytics** designed for detection and tracking of vehicles and license plates in a video stream or detection in images. The main functions of the service are presented below:

* Display of vehicle and license plate detection events;

* Setting up lists for creating incidents;

* Display of incidents;

* Search for events by incidents;

* Management of user accounts and their access rights;

* Viewing processed video streams from cameras;

* Creation of tasks for search by image and export of search results to a «.xlsx» file.

System Requirements
--------------------------

Several requirements and conditions must be considered when preparing the installation and operation of the CARS Analytics, as indicated below. The list of system requirements is presented in Table 1.

**Table 1**. System requirements

+---------------------------+-----------------------------------------------------------+
|**Resources**              |**Recommended**                                            |
+===========================+===========================================================+
|CPU                        |Intel 4 Cores 2,0 GHz                                      |                          
+---------------------------+-----------------------------------------------------------+
|RAM                        |8 GB or higher                                             |         
+---------------------------+-----------------------------------------------------------+
|HDD or SSD                 |10 GB or higher *                                          |
+---------------------------+-----------------------------------------------------------+
|OS                         |CentOS 7.4 x86_64 **                                       |                       
+---------------------------+-----------------------------------------------------------+
|Browser                    |Microsoft Edge (44.0 and up);                              |
|                           |                                                           |    
|                           |Mozilla Firefox (60.3.0 and up);                           |    
|                           |                                                           |    
|                           |Google Chrome (50.0 and up).                               |    
+---------------------------+-----------------------------------------------------------+

(*) The specified amount of disk space does not include the space for storing images of vehicles and license plates.

(**) Any other similar OS with Python 3.7 support can be used (Ubuntu, Debian etc.).

1. Installation
----------------------

The CARS Analytics distribution is a «CARS.ANALYTICS_v.*.zip» archive. The archive contains scripts for installing Ansible, as well as distributions of frontend and backend components.

The archive contains components required for installation of CARS Analytics. 

.. note:: The archive does not include CARS API and CARS Stream, which are required for the full operation of the system. Also, the archive does not include some dependencies that are included in the standard distribution of the CentOS repository, they can be downloaded from open sources during the installation process.

Installation of the system is possible with Ansible and by deploying Docker containers.

.. note:: Entire configuration and installation process must be performed under a superuser account (with root rights).

Before installing, unpack the archive and place the distribution files in a separate folder on the server.

Install the archiver if it is not installed: ::

    yum install unzip-6.0-21.el7

.. note:: It is recommended to use the specified version of the archiver to avoid errors during installation.

Unzip archive: ::

    unzip <cars.analytics>.zip -d ./<cars.analytics>

1.1. Installation with Ansible
''''''''''''''''''''''''''''''''

1.1.1. Pre-install Process
"""""""""""""""""""""""""""""""""""
You need to install the Ansible package by running the commands.

Package manager update: ::

    yum update

Additional repositories install: ::

    yum install epel-release

Ansible installation: ::

    yum install ansible

1.1.2. Settings SSH
""""""""""""""""""""""""""""""""""""""""""""""""""

On the source server, you need to generate an SSH-key and add it to the target server. First you need to check and configure the SSH-service: ::

    systemctl status sshd

Run servise if SSH ended: ::

    systemctl start sshd

Install server if needed: ::
    
    yum install -y openssh-server

After that, you need to generate a key by running the command: ::

    ssh-keygen

If necessary, you can specify a passphrase or leave it blank. To copy the key to the target server, enter the command: ::

    ssh-copy-id username@hostname

«username» is the name of the authorized user and «hostname» is the IP-address of the target server.

.. note:: This method is not the only one possible. You can use any other convenient method to provide SSH bridge between the installer server and the target server.

1.1.3. Settings of «hosts» Configuration File
""""""""""""""""""""""""""""""""""""""""""""""""""

The archive contains the «hosts» file. This file is in the /ansible directory. You need to set the external IP-address of the server where the application will be installed. If the installation will be performed locally using a distribution that is located on an external machine, then you must specify the IP_address of the local machine.

.. note:: Installation can be performed locally, while it is necessary to enter an external IP address and access via SSH.

::

    #frontend part
    [frontend]  
    <IP_address>

    #Only 1 host
    #DB ip-address
    [postgres]
    <IP_address>

    #Only 1 host
    #ip-address redis
    [redis]
    <IP_address>

    #CARS API
    #Multiple hosts allowed
    #ip-address CARS API
    [api]
    <IP_address>

    #Only 1 host
    #NGINX ip-address
    [nginx]
    <IP_address>

    #CARS Stream
    #Only 1 host
    #ip-address CARS Stream
    [stream]
    <IP_address>

1.1.4. Settings of «all.yml» Configuration File
""""""""""""""""""""""""""""""""""""""""""""""""""""""""

It is necessary to configure the service in the «all.yml» configuration file located in the /ansible/group_vars directory. Description of the parameters is in the Table 2.

**Table 2**. Parameters of «all.yml» configuration file 

+---+----------------------+----------------------------------------------------------------+
|#  |**Parameter**         | **Description**                                                |
+===+======================+================================================================+
|1  |firewall_disable      |The parameter responsible for disabling the firewall.           |
|   |                      |                                                                |
|   |                      |Possible values:                                                |
|   |                      |                                                                |
|   |                      | - yes – firewall off;                                          |
|   |                      |                                                                |    
|   |                      | - no – firewall on.                                            |
+---+----------------------+----------------------------------------------------------------+
|2  |selinux_disable       |The parameter responsible for disabling the forced access       |
|   |                      |                                                                |
|   |                      | control system «SELinux». Possible Values:                     |
|   |                      |                                                                |
|   |                      | - yes – SELinux off;                                           |
|   |                      |                                                                |    
|   |                      | - no – SELinux on.                                             |
+---+----------------------+----------------------------------------------------------------+
|3  |packages_update       |The parameter responsible for disabling package updates during  |
|   |                      |                                                                |
|   |                      |installation. Possible Values:                                  |
|   |                      |                                                                |
|   |                      | - yes – update on;                                             |
|   |                      |                                                                |    
|   |                      | - no – update off.                                             |
+---+----------------------+----------------------------------------------------------------+

.. note:: When SELinux is off, the server will reboot!

The section for configuring main parameters is shown in Table 3.

**Table 3**. Main parameters of the system

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |LUNA_CARS_HOME        |This is the directory where the installation will take place. It is|
|   |                      |                                                                   |
|   |                      |recommended to leave the default path to the home directory,       |
|   |                      |                                                                   |
|   |                      |otherwise there may be directory search errors during installation.|
|   |                      |                                                                   |
|   |                      |Default value: /var/lib/luna/CARS                                  |
+---+----------------------+-------------------------------------------------------------------+
|2  |HASP_license_server   |Specifies the path to the server to which the installer calls for  |
|   |                      |                                                                   |
|   |                      |an available network product license. If there is no network       |
|   |                      |                                                                   |
|   |                      |license, then it must be specified locally.                        |
+---+----------------------+-------------------------------------------------------------------+
|3  |HASP_wait_time        |Specifies the time to response when a request for an available     |
|   |                      |                                                                   |
|   |                      |license is sent to the server. Set in minutes.                     |
+---+----------------------+-------------------------------------------------------------------+
|4  |Emirates              |Selecting country for license plate recognition. Possible values   |
|   |                      |                                                                   |
|   |                      |* true – system will recognize only UAE license plate;             |
|   |                      |                                                                   |
|   |                      |* false – system will recognize license plates of RF, CIS and EU.  |
+---+----------------------+-------------------------------------------------------------------+
|5  |TIME_ZONE             |Parameter set time on the server. The value of this parameter can  |
|   |                      |                                                                   |
|   |                      |be any of the values available in the timezone database.           |
+---+----------------------+-------------------------------------------------------------------+
|6  |luna_cars_vers        |Specifies the name of archive. For example, luna-cars_v.0.0.12.    |
+---+----------------------+-------------------------------------------------------------------+
|7  |luna_cars_zip_location|This parameter sets the path to the CARS.Stream archive.           |
|   |                      |                                                                   |
|   |                      |For example, /distr/api/<luna-cars_v.0.0.12>.                      |
+---+----------------------+-------------------------------------------------------------------+
|8  |luna_cs_vers          |Specifies name of the archive. For example, carstream_linux_v.*.   |
+---+----------------------+-------------------------------------------------------------------+
|9  |luna_cs_zip_location  |This parameter sets the path to the CARS Stream archive. For       |
|   |                      |                                                                   |
|   |                      |example, /distr/api/carstream_linux_v.*.                           |
+---+----------------------+-------------------------------------------------------------------+

The section for configuring ports is shown in Table 4.

**Table 4**. Main ports of the system

+---+-----------------------+------------------------------------------------------------------+
|#  |**Parameter**          |**Description**                                                   |
+===+=======================+==================================================================+
|1  |CARS_API_PORT: 8100    |First port of CARS API.                                           |
+---+-----------------------+------------------------------------------------------------------+
|2  |CARS_API_PORT_RANGE: 2 |CARS API port range.                                              |
+---+-----------------------+------------------------------------------------------------------+
|3  |CARS_API_NGINX_PORT: 81|NGINX balancer port.                                              |
+---+-----------------------+------------------------------------------------------------------+
|4  |CARS_BACK_PORT: 8000   |Backend port.                                                     |
+---+-----------------------+------------------------------------------------------------------+
|5  |CARS_FRONT_PORT: 8000  |Frontend port.                                                    |
+---+-----------------------+------------------------------------------------------------------+

The section for configuring additional parameters is shown in Table 5.

**Table 5**. Addition parameters of the system

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |FRONTEND_PROTOCOL     |Specifies the communication parameter for internal services. As a  |
|   |                      |                                                                   |
|   |                      |rule, communication takes place via the «http» protocol.           |
+---+----------------------+-------------------------------------------------------------------+
|2  |LOGGING_DIR           |Specifies the name of the logs folder. Default location of this    |
|   |                      |                                                                   |
|   |                      |folder: /var/lib/luna/cars/back/.                                  |
+---+----------------------+-------------------------------------------------------------------+
|3  |DB_NAME               |Specifies name of database.                                        |
+---+----------------------+-------------------------------------------------------------------+
|4  |DB_USER               |Specifies login of database user.                                  |
+---+----------------------+-------------------------------------------------------------------+
|5  |DB_PASSWORD           |Specifies password of database user.                               |
+---+----------------------+-------------------------------------------------------------------+
|6  |DB_PORT: 5432         |Database port.                                                     |
+---+----------------------+-------------------------------------------------------------------+
|7  |REDIS_PORT: 6379      |«Redis» queue manager port.                                        |
+---+----------------------+-------------------------------------------------------------------+
|8  |CAMERA_CHECK_INTERVAL |Sets the frequency at which the system will poll camera status.    |
|   |                      |                                                                   |
|   |                      |The value is set in minutes.                                       |
+---+----------------------+-------------------------------------------------------------------+
|9  |CAMERA_RESTART_FAILED |Sets the frequency at which the system will try to restart the     |
|   |                      |                                                                   |
|   |_INTERVAL             |camera if no response was received from the camera when checking   |
|   |                      |                                                                   |
|   |                      |status. The value is set in minutes.                               |
+---+----------------------+-------------------------------------------------------------------+
|10 |ADMIN_NAME            |Specifies the name of a user with the administrator role (system   |
|   |                      |                                                                   |
|   |                      |superuser). A user with the specified role is needed to configure  |
|   |                      |                                                                   |
|   |                      |and debug the system.                                              |
+---+----------------------+-------------------------------------------------------------------+
|11 |ADMIN_EMAIL           |Email of administrator.                                            |
+---+----------------------+-------------------------------------------------------------------+
|12 |ADMIN_PASSWORD        |Password of administrator.                                         |
+---+----------------------+-------------------------------------------------------------------+
|13 |DEBUG                 |Enabling debug mode.                                               |
+---+----------------------+-------------------------------------------------------------------+
|14 |SECRET_KEY_FOLDER     |Specifies the folder where the «secret_key.txt» file will be       |
|   |                      |                                                                   |
|   |                      |placed. By default, the file is generated in the                   |
|   |                      |                                                                   |
|   |                      |/var/lib/luna/cars/back/data directory.                            |
+---+----------------------+-------------------------------------------------------------------+

1.1.4.1. Mail Service Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The section for configuring mail service parameters is shown in Table 6.

These parameters are required to configure the mail service, the parameter values are taken from open sources.

**Table 6**. Mail service parameters

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |EMAIL_USE_TLS         |Specifies whether to use TLS (secure) connection with the SMTP     |
|   |                      |                                                                   |
|   |                      |server. This used to explicitly use of TLS connection.             |
+---+----------------------+-------------------------------------------------------------------+
|2  |EMAIL_PORT            |Specifies the port used when connecting to the SMTP server.        |
+---+----------------------+-------------------------------------------------------------------+
|3  |EMAIL_HOST            |Specifies the hostname used to send system emails.                 |
+---+----------------------+-------------------------------------------------------------------+
|4  |EMAIL_HOST_USER       |Sets the username used when connecting to the SMTP server specified|
|   |                      |                                                                   |
|   |                      |in EMAIL_HOST.                                                     |
+---+----------------------+-------------------------------------------------------------------+
|5  |SERVER_EMAIL          |Specifies the Email address used as the sender's address.          |
|   |                      |                                                                   |
|   |                      |By default, is taken from the EMAIL_HOST_USER parameter.           |
+---+----------------------+-------------------------------------------------------------------+
|6  |DEFAULT_FROM_EMAIL    |Specifies the Email used as automatic mailings.                    |
+---+----------------------+-------------------------------------------------------------------+
|7  |EMAIL_HOST_PASSWORD   |Specifies the password for connecting to the SMTP server.          |
|   |                      |                                                                   |
|   |                      |This setting is used in conjunction with EMAIL_HOST_USER for       |
|   |                      |                                                                   |
|   |                      |authorization on SMTP server.                                      |
+---+----------------------+-------------------------------------------------------------------+

When you are working with Gmail and Yandex mail services without 2-step authentication, you must give permission to work with unsafe applications.

1.1.4.2. Setting up integration with backend
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The CARS Analytics distribution contains the archive «cars_analytics_backend.zip» in the /distr/back directory. The /docs directory contains the instruction «restapi_backend.html» for integration with the backend.

1.1.5. Starting Installation with Ansible
"""""""""""""""""""""""""""""""""""""""""""""

To start the installation, you must be in /ansible/ directory and execute the following command to start the installation procedure: ::

    ansible-playbook -i hosts install_analytics.yml

You can enter the CARS Analytics by typing in your browser http://<IP_address>:8080. The IP address was specified in the «hosts» file in section 1.1.3.

1.2. Installation with Docker
''''''''''''''''''''''''''''''''''''''''

1.2.1. Installation Docker and Docker-compose
""""""""""""""""""""""""""""""""""""""""""""""

You can use the official `instruction <https://docs.docker.com/engine/install/centos/>`_ for CentOS, as this instruction is the most up to date.

Additional dependencies: ::

    sudo yum install -y yum-utils

Add docker repository: ::

    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo

Docker installations: ::

    sudo yum install docker-ce docker-ce-cli containerd.io

Installation check: ::

    docker -v

Download docker-compose: ::

    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Assigning an Executable File Property: ::
    
    sudo chmod +x /usr/local/bin/docker-compose

1.2.2. CARS API and CARS Stream
""""""""""""""""""""""""""""""""""""""""""""

The CARS API and CARS Stream archives must be placed in the appropriate directories. The «luna-cars_v.zip» distribution should be placed in /distr/api and» carstream_linux_v.zip» should be placed in /distr/stream.

1.2.3. Settings of «config.conf» Configuration File
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Before starting the installation, you need to configure the service in the «config.conf» file. This file located in the /distr/back directory. It is necessary to configure the mail mechanism, without it will be impossible to create a new user. By default, a test user will be created - an administrator, his parameters are specified in the configuration section. The parameter list divided into several sections. The general service parameters section is shown in Table 7.

**Table 7**. Main parameters of the service

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |LANGUAGE_CODE         |System language. Possible Values:                                  |
|   |                      |                                                                   |
|   |                      | - ‘ru-ru’ – Russian UI;                                           |
|   |                      |                                                                   |
|   |                      | - ‘en-us’ – English UI.                                           |
+---+----------------------+-------------------------------------------------------------------+
|2  |TIME_ZONE             |Parameter set time on the server. The value of this parameter can  |
|   |                      |                                                                   |
|   |                      |be any of the values available in the timezone database.           |
+---+----------------------+-------------------------------------------------------------------+
|3  |ADMIN_NAME            |Specifies the name of a user with the administrator role (system   |
|   |                      |                                                                   |
|   |                      |superuser). A user with the specified role is needed to configure  |
|   |                      |                                                                   |
|   |                      |and debug the system.                                              |
+---+----------------------+-------------------------------------------------------------------+
|4  |ADMIN_EMAIL           |Email of administrator.                                            |
+---+----------------------+-------------------------------------------------------------------+
|5  |ADMIN_PASSWORD        |Password of administrator.                                         |
+---+----------------------+-------------------------------------------------------------------+
|6  |FRONTEND_PROTOCOL     |Specifies the communication parameter for internal services. As a  |
|   |                      |                                                                   |
|   |                      |rule, communication takes place via the «http» protocol.           |
+---+----------------------+-------------------------------------------------------------------+
|7  |FRONTEND_URL          |Specifies the connection path to the system.                       |
|   |                      |                                                                   |
|   |                      |The default is front:8080                                          |
+---+----------------------+-------------------------------------------------------------------+
|8  |CAMERA_CHECK_INTERVAL |Sets the frequency at which the system will poll camera status.    |
|   |                      |                                                                   |
|   |                      |The value is set in minutes.                                       |
+---+----------------------+-------------------------------------------------------------------+
|9  |CAMERA_RESTART_FAILED |Sets the frequency at which the system will try to restart the     |
|   |                      |                                                                   |
|   |_INTERVAL             |camera if no response was received from the camera when checking   |
|   |                      |                                                                   |
|   |                      |status. The value is set in minutes.                               |
+---+----------------------+-------------------------------------------------------------------+

The additional service parameters section is shown in Table 8.

**Table 8**. Main parameters of the system

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |LOGGING_DIR           |Specifies the name of the logs folder. Default location of this    |
|   |                      |                                                                   |
|   |                      |folder: /var/lib/luna/cars/back/.                                  |
+---+----------------------+-------------------------------------------------------------------+
|2  |DB_NAME               |Specifies name of database.                                        |
+---+----------------------+-------------------------------------------------------------------+
|3  |DB_USER               |Specifies login of database user.                                  |
+---+----------------------+-------------------------------------------------------------------+
|4  |DB_PASSWORD           |Specifies password of database user.                               |
+---+----------------------+-------------------------------------------------------------------+
|5  |DB_HOST               |Database host name                                                 |
+---+----------------------+-------------------------------------------------------------------+
|6  |DB_PORT: 5432         |Database port.                                                     |
+---+----------------------+-------------------------------------------------------------------+
|7  |REDIS_HOST            |Redis host name.                                                   |
+---+----------------------+-------------------------------------------------------------------+
|8  |REDIS_PORT: 6379      |«Redis» queue manager port.                                        |
+---+----------------------+-------------------------------------------------------------------+
|9  |DEBUG                 |Enabling debug mode.                                               |
+---+----------------------+-------------------------------------------------------------------+
|10 |SECRET_KEY_FOLDER     |Specifies the folder where the «secret_key.txt» file will be       |
|   |                      |                                                                   |
|   |                      |placed. By default, the file is generated in the                   |
|   |                      |                                                                   |
|   |                      |/var/lib/luna/cars/back/data directory.                            |
+---+----------------------+-------------------------------------------------------------------+

The parameters of the environment file «.env» are shown in Table 9.

**Table 9**. Environment file parameters

+---+-------------------+-------------------------------------------------------------------+
|#  |**Parameter**      | **Description**                                                   |
+===+===================+===================================================================+
|1  |HASP_license_server|Specifies the path to the server to which the installer calls for  |
|   |                   |                                                                   |
|   |                   |an available network product license. If there is no network       |
|   |                   |                                                                   |
|   |                   |license, then it must be specified locally.                        |
+---+-------------------+-------------------------------------------------------------------+
|2  |HASP_wait_time     |Specifies the time to response when a request for an available     |
|   |                   |                                                                   |
|   |                   |license is sent to the server. Set in minutes.                     |
+---+-------------------+-------------------------------------------------------------------+
|3  |Emirates           |Selecting country for license plate recognition. Possible values   |
|   |                   |                                                                   |
|   |                   |* true – system will recognize only UAE license plate;             |
|   |                   |                                                                   |
|   |                   |* false – system will recognize license plates of RF, CIS and EU.  |
+---+-------------------+-------------------------------------------------------------------+
|4  |ENG                |Specifies service language. Possible values:                       |
|   |                   |                                                                   |
|   |                   |* true – English UI;                                               |
|   |                   |                                                                   |
|   |                   |* false – Russian UI.                                              |
+---+-------------------+-------------------------------------------------------------------+

1.2.3.1. Mail Service Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The section for configuring mail service parameters is shown in Table 10.

These parameters are required to configure the mail service, the parameter values are taken from open sources.

**Table 10**. Mail service parameters

+---+----------------------+-------------------------------------------------------------------+
|#  |**Parameter**         |**Description**                                                    |
+===+======================+===================================================================+
|1  |EMAIL_USE_TLS         |Specifies whether to use TLS (secure) connection with the SMTP     |
|   |                      |                                                                   |
|   |                      |server. This used to explicitly use of TLS connection.             |
+---+----------------------+-------------------------------------------------------------------+
|2  |EMAIL_PORT            |Specifies the port used when connecting to the SMTP server.        |
+---+----------------------+-------------------------------------------------------------------+
|3  |EMAIL_HOST            |Specifies the hostname used to send system emails.                 |
+---+----------------------+-------------------------------------------------------------------+
|4  |EMAIL_HOST_USER       |Sets the username used when connecting to the SMTP server specified|
|   |                      |                                                                   |
|   |                      |in EMAIL_HOST.                                                     |
+---+----------------------+-------------------------------------------------------------------+
|5  |SERVER_EMAIL          |Specifies the Email address used as the sender's address.          |
|   |                      |                                                                   |
|   |                      |By default, is taken from the EMAIL_HOST_USER parameter.           |
+---+----------------------+-------------------------------------------------------------------+
|6  |DEFAULT_FROM_EMAIL    |Specifies the Email used as automatic mailings.                    |
+---+----------------------+-------------------------------------------------------------------+
|7  |EMAIL_HOST_PASSWORD   |Specifies the password for connecting to the SMTP server.          |
|   |                      |                                                                   |
|   |                      |This setting is used in conjunction with EMAIL_HOST_USER for       |
|   |                      |                                                                   |
|   |                      |authorization on SMTP server.                                      |
+---+----------------------+-------------------------------------------------------------------+

.. note:: When you are working with Gmail and Yandex mail services without 2-step authentication, you must give permission to work with unsafe applications.

1.2.4. Starting Installation
"""""""""""""""""""""""""""""""""

Move to the created directory by running the command: ::

    cd <cars.analytics>

You need to make sure that the port designated for CARS Analytics is not in use:

    ss -ltn | fgrep 8080

The system will respond if the port is busy. In this case, you need to contact the system administrator to resolve the issue of freeing the port, or use another available one by editing the value of the corresponding ports variable in the docker-compose.yml file: ::

    front:
        ports:
          - 8080:8080

It is also recommended to check the ports for the HASP licensing service. The default ports are 1948-1950.

Start the installation process: ::

    docker-compose up -d

You can enter the CARS Analytics by typing in your browser http://<IP_address>:8080. The IP address was specified in the «FRONTEND_URL» parameter in Table 7.

2. Systems Control Panel
----------------------------------

2.1. Administration Interface
''''''''''''''''''''''''''''''''''''

The system has an administrator interface for viewing and managing objects in the database (Figure 1). To enter it, you need to go in a browser to the following address http://<IP-address>:8080/admin/. Authorization is carried out using the login and password of the CARS Analytics administrator.

In the upper part of the window there is a quick access panel. It contains following objects:

* administrator's login;

* link to the graphical web-interface;

* link to the administrator's password change page;

* logout button.

Basic actions available on the main page of the CARS Analytics administration section:

* Adding users without confirmation by Email;

* Change of fields in the user's card;

* Removing users;

* Adding cameras and changing camera settings;

* Adding lists and editing list attributes;

* Editing of displays of names of colors and countries;

* Viewing tasks, the results of their execution and exporting files with the result of the task

.. note:: It is recommended to create users through the administrator interface only if there is no Internet connection.

The described actions are performed by pressing the «Change», «View» or «Add» buttons.

On the right side of the administration interface, there is a log of the latest actions in the system.

2.2. System Configuration
''''''''''''''''''''''''''''''''''''

2.2.1. Camera Preview
""""""""""""""""""""""""""""""""""""""""""""""""""""""

The camera preview allows you to configure the detection and recognition zones.

To add an image to the camera preview, you must perform the following steps: 

1.	You need to prepare the image. It is recommended to take a screenshot of the video stream in its original resolution.

2.	Go to the section http://<IP_address>:8080/admin/core_app/camera/ and select the camera.

3.	In the «Status» section in the «Image with preview» item, click the «Add file» button (Figure 2).

4.	Selecting photo

5.	Save the changes by pressing «Save» button.

2.2.2. Camera Location
""""""""""""""""""""""""""""""""""""""""""""""""""""""

To add camera location, you must perform the following steps:

1. You need to move to http://<IP_address>:8080/admin/core_app/camera/ and select the camera.

2. In the «Location» section, fill in all the fields (Figure 4).

.. note:: The «Address» field is optional.

3. Save the changes by pressing «Save» button.

The search on the map is based on the values of longitude and latitude.

Based on the filled data, it is possible to display the location of the camera on the map.

2.3. Testing CARS API in CARS Analytics web-interface.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

It is possible to use an interface that allows you to manually send a vehicle image for processing.

The system supports images in «.png» or «.jpg» format, less than 2.5 Mb.

By default, after installation, the application is deployed to http://<IP_address>:8080/cars_api_tester.

Testing of the following queries is available:

* Classify - request for classification;

* Detect - request for detection;

* Frame processing - request for frame processing.

After logging in, the interface shown in Figure 5 is available.

The interface contains a field with an IP address where the CARS API application is located, available classifiers and detectors.

To execute a request for classification or detection, click the corresponding button.

2.3.1. «Classify» Request
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

The «Classify» query allows you to classify a vehicle or a license plate.

After clicking on the «Classify» button, interface shown in Figure 6 will be displayed.

The respond includes the result of image recognition according to the given classifiers.

2.3.2. «Detect» Request
""""""""""""""""""""""""""""""""""""""""""""""""""""""

The «Detect» request detects vehicle and license plate on the image. System sends the main attributes of the detected object (coordinates and bbox sizes).

After clicking on «detect» button, the interface shown in Figure 11 will be displayed.

In step 1 «Choose an image to detect object on them», it is necessary to load the vehicle image. To download an image, you need to click on the download area and select the image in the explorer window that appears. Loading and deleting an image occurs in the same way as in section 2.3.1 and is presented in Figure 7 and Figure 8..

In step 2 «Choose detectors», you need to select the detectors.

In step 3 «Send request to CARS API», press «Send» to finish request.

If you want to display the bbox, check the «visualize detectors» checkbox.

After finishing processing, the service will respond (Figure 12).

The respond includes the coordinates of bbox, visualization with the applied bbox and a fragment of the image with a detected object or fragments, if the original image had several vehicles or LPs.

2.3.3. «Frame processing» Request
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Sending a «Frame processing» request will emulate the system operation for one image. Detection will be performed for all classifiers.

After clicking on the «Classify» button, the query options displayed in Figure 13 will appear.

In step 1 «Choose an image to detect object on them», it is necessary to load the vehicle image. To download an image, you need to click on the download area and select the image in the explorer window that appears. Loading  image is similar to section 2.3.1 and is presented in Figure 7 and Figure 8.

When the «Save as Event» flag is activated, the response is saved as an event (it will be displayed in the «Events» section of the CARS Analytics web interface).

When the «Detect GRZ only» flag is activated, only license plates will be detected.

In step 2 «Choose Camera name», you must enter the name of the camera. This name will be transfer to the system when the response is saved as an event.

In step 3 «Send for processing», a request is sent to CARS API. To send a request, click the «Send!» Button.

After processing the image, the service will return the response shown in Figure 14.