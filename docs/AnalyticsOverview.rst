System Overview
=========================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Detection          |Actions to determine areas of the image that contain vehicle or LP. |
+-------------------+--------------------------------------------------------------------+
|Event              |Detected vehicle with extracted attributes on a specific source.    |
+-------------------+--------------------------------------------------------------------+
|Incident           |Event founded in lists.                                             |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
-----------------

This document provides a general description of the LUNA CARS Analytics version 2.0.9.

The document is intended to explain to the user basic functionality of the system for collecting and analyzing detection events and recognition of the vehicle and the license plate.

General information
------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs CARS Analytics** designed for detection and tracking of vehicles and license plates in a video stream or detection in images. The main functions of the service are presented below:

* Display of vehicle and license plate detection events;

* Setting up lists for creating incidents;

* Display of incidents;

* Search for events by incidents;

* Management of user accounts and their access rights;

* Viewing processed video streams from cameras;

* Creation of tasks for search by image and export of search results to a «.xlsx» file.

1. Service Description
-------------------------------------------

Users interact with the system via web-browser.

CARS Analytics is supported in the following browser versions:

* Microsoft Edge (44.0 or newer);

* Mozilla Firefox (60.3.0 or newer);

* Google Chrome (50.0 or newer).

The general scheme of operation of LUNA CARS products is shown in Figure 1.
 
1. The administrator creates a camera in the CARS Analytics interface as a video stream source. At the specified address in CARS  Stream frames of the video stream are sent for the detection of the vehicle and the license plate.

2. CARS Stream server can receive frames from the specified source: an RTSP video stream, a video file or a set of vehicle images.

3. CARS Stream server processes incoming data, detects the vehicle and license plate, selects the best shot and sends the areas with the vehicle and license plate with classifier to CARS API.

4. CARS API processes incoming images according to the parameters passed in the request from CARS Stream. The results of processing are stored in log files. CARS API returns to the CARS Stream server results of processing images sent earlier by the classifiers specified in CARS Stream.

5. CARS Stream aggregates data from classifiers within one track and updates the data if the new best shot is more accurate. Aggregated data from CARS API is transferred via CARS Stream to CARS Analytics and stored in the database.

6. The detection results are displayed in a convenient form on the CARS Analytics service in the user’s web-browser.

