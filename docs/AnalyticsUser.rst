User Manual
=========================

Glossary
-----------------

+-------------------+--------------------------------------------------------------------+
|**Term**           |**Description**                                                     |
+===================+====================================================================+
|Best shot          |A frame of a video stream on which a vehicle or a license plate is  |
|                   |                                                                    |
|                   |detected in the optimal perspective for further processing.         |
+-------------------+--------------------------------------------------------------------+
|Classifier         |A system parameter that recognizes one of the vehicle or license    |
|                   |                                                                    |
|                   |plate attributes.                                                   |
+-------------------+--------------------------------------------------------------------+
|Detection          |Actions to determine areas of the image that contain vehicle or LP. |
+-------------------+--------------------------------------------------------------------+
|Event              |Detected vehicle with extracted attributes on a specific source.    |
+-------------------+--------------------------------------------------------------------+
|Incident           |Event founded in lists.                                             |
+-------------------+--------------------------------------------------------------------+
|LP (License plate) |A vehicle registration plate.                                       |
+-------------------+--------------------------------------------------------------------+

Introduction
----------------------

This document is manual for user of the service for displaying the results of CARS API and CARS Stream work through the CARS Analytics 2.0.9 web interface.

This manual describes the operation and administration of the service.

It is recommended to carefully read this manual before installing and using the service.

General information
------------------------

**VisionLabs LUNA CARS** is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS Analytics, CARS API and CARS Stream.

**VisionLabs CARS Analytics** designed for detection and tracking of vehicles and license plates in a video stream or detection in images. The main functions of the service are presented below:

* Display of vehicle and license plate detection events;

* Setting up lists for creating incidents;

* Display of incidents;

* Search for events by incidents;

* Management of user accounts and their access rights;

* Viewing processed video streams from cameras;

* Creation of tasks for search by image and export of search results to a «.xlsx» file.

System Requirements
--------------------------------

The user operates in the service in a web browser. The hardware requirements are listed in Table 1. The software requirements are listed in Table 2.

It is not recommended to use monitors with a resolution less than 1600x1200.

**Table 1**. Hardware requirements

+-------------------+---------------------------------------------------------------------------+
|**Resource**       |**Recommended**                                                            |
+===================+===========================================================================+
|CPU                |Intel Atom, Intel Core 2 DUO, Pentium G6, Celeron M P45/U34, AMD Athlon x2 |
|                   |                                                                           |
|                   |or newer models with SSE2 support, 2 cores or more, 1.1 GHz or higher.     |           
+-------------------+---------------------------------------------------------------------------+
|RAM                |8 GB or higher                                                             |         
+-------------------+---------------------------------------------------------------------------+
|HDD or SSD         |10 GB or higher *                                                          |
+-------------------+---------------------------------------------------------------------------+

**Table 2**. Software requirements

+-------------------+---------------------------------------------------------------------------+
|**Resource**       |**Recommended**                                                            |
+===================+===========================================================================+
|OS                 |Windows 7/8/8.1/10;                                                        |
|                   |                                                                           |
|                   |macOS 10.12 and up;                                                        |
|                   |                                                                           |
|                   |CentOS 7 and up;                                                           |
|                   |                                                                           |
|                   |Ubuntu 14.05 (x64) and up.                                                 |                        
+-------------------+---------------------------------------------------------------------------+
|Browser            |Microsoft Edge (44.0 and up);                                              |
|                   |                                                                           |    
|                   |Mozilla Firefox (60.3.0 and up);                                           |    
|                   |                                                                           |    
|                   |Google Chrome (50.0 and up).                                               |    
+-------------------+---------------------------------------------------------------------------+
|Internet connection|Stable internet connection with stable data transfer rate at least 1 Mb/s. |  
+-------------------+---------------------------------------------------------------------------+

1. Work with Service
--------------------------------

All users are created by administrator, the user receives a link to continue registration by email.

1.1. Role Model
'''''''''''''''''''''''''''''''''

Role model is presented in Luna CARS  Now there are 2 roles – user and user with administrator role (administrator). The list of available functions for each role is presented in Table 3.

**Table 3**. Functions by Roles

+--------------+---------------------------------------------+------------+-------------+
|Tab           |Action/Function                              |User        |Administrator|
+==============+=============================================+============+=============+
|Events        |Show all events                              |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Event filter                                 |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Event export                                 |Yes         |Yes          |
+--------------+---------------------------------------------+------------+-------------+
|Lists         |Show all lists                               |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Show all attributes                          |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |List record filter                           |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Add/Edit/Delete list                         |No          |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Add/Edit/Delete list records                 |No          |Yes          |
+--------------+---------------------------------------------+------------+-------------+
|Incidents     |Show all incidents                           |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Incident filter                              |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Incident export                              |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Read Incidents                               |Yes         |Yes          |
+--------------+---------------------------------------------+------------+-------------+
|Cameras       |Show all cameras                             |Yes         |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Add/Edit/Delete camera                       |No          |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Restart stream                               |No          |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Show stream                                  |Yes         |Yes          |
+--------------+---------------------------------------------+------------+-------------+
|Users         |Access to tab                                |No          |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Show all users                               |No          |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |User management                              |No          |Yes          |
+--------------+---------------------------------------------+------------+-------------+
|Tasks         |Show result                                  |Yes *       |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Delete result                                |Yes *       |Yes *        |
|              +---------------------------------------------+------------+-------------+
|              |Search for task                              |Yes *       |Yes          |
|              +---------------------------------------------+------------+-------------+
|              |Pause task                                   |Yes *       |Yes          |
+--------------+---------------------------------------------+------------+-------------+

* These parameters can be changed only by creator.

1.2. Authorization
'''''''''''''''''''''''''''''''''

The system is accessed by logging into a web browser. The link to enter the CARS Analytics web interface must be requested from the system administrator.

1.2.1. Account Activating
""""""""""""""""""""""""""""""""""""""

An example of a registration letter is shown in Figure 1.

After successfully creating a password, the system will automatically prompt you to log in.

1.2.2. Log In
""""""""""""""""""""""""""""""""""""""

When you enter the service, an authorization form is launched. For authorization in the service, you must enter your credentials (Email and password) in the appropriate fields and click the «Login» button (Figure 3).

.. note:: The administrator is created automatically during installation and is used for primary access to the system. The login and password data are set in the configuration file «all.yml», by default the login is «admin@test.ru», the password is «test_admin», if the values of the main parameters in the file «all.yml» have not been changed. In the future, this user can be deleted.

1.2.3. Password Reset
""""""""""""""""""""""""""""""""""""""

The password can be reset from the login page by clicking the corresponding button (Figure 4).

Letter will be sent to the mail, in which you will need to follow the link and enter a new password. This process is similar to the account activation process (section 1.2.1).

1.2.4. Log Out
""""""""""""""""""""""""""""""""""""""

To logout from service, click the «Logout» button, which is located in the drop-down list to the right of the user image (Figure 6).

By clicking on this button, the user is taken to the authorization form in the service (Figure 3).

2. Service Tabs
-----------------------------

After successful authorization, the user goes to the «Events» tab (Figure 7).

The user interface contains 6 tabs, switching between which is made in the upper menu bar. This line contains buttons for navigating through all sections:

* «Events» tab - the section that displays the events recorded by the system;

* «Incidents» tab - the section where incidents are displayed;

* «Lists» tab - a section for creating, viewing and editing lists;

* «Cameras» tab - the section is used to display the status of cameras and configure the parameters of the video stream for each of their cameras. Parameter setting is available only to the administrator;

* «Users» tab - section for managing user accounts. This is admin section;

* «Tasks» tab - the section allows you to view the list of tasks, the status of their execution and download the result of the task.

General view of the top menu bar for administrator and user is shown in Figures 8 and 9.

In the upper right corner, there is a user image and a drop-down list containing buttons to go to the «Tasks» tab and a button to log out.

Under the menu, their content is presented.

2.1. «Events» Tab
''''''''''''''''''''''''''''''

The «Events» tab is intended for viewing events. Elements of the «Events» interface are shown in Figure 10.

Tab consists of following elements:

* Form for filtering events;

* Table of events;

* Pagination buttons.

Filters are disabled by default, and all new events are automatically loaded into the table.

The event table contains the following fields:

* Camera – source name, which detected event;

* Time – time of registration;

* Type – vehicle category attribute;

* Brand – vehicle brand attribute;

* Model – vehicle model attribute;

* Emergency service – vehicles belonging to emergency service attribute;

* Color – vehicle color attribute;

* LP country – country of vehicle registration attribute;

* LP number – characters on vehicles license plate attribute;

* Vehicle photo – thumbnail with a frame of the vehicle, you can open it in a new tab by clicking on photo;

* LP photo – thumbnail with a frame of the license plate, you can open it in a new tab by clicking on photo;

* Track ID – vehicle track number;

* Frame ID – vehicle frame number.

.. note:: The red numbers for each attribute are an assessment of the recognition quality of this attribute.

2.1.1. Event Filter
""""""""""""""""""""""""""""""""""""""

Filtering events is possible by one or many attributes at the same time (Figure 11). Some fields support multiple selection.

A description of each field is provided below:

* Plate Number. This field is for entering the number of the license plate.

* Types. You must select the type of vehicle.

* Brands. Field for selecting vehicle brands.

* Models. Field for selecting the vehicle model.

* Colors. This field is used to select the vehicle color.

* Emergency services. Field for selecting emergency services.

* Countries. Countries are selected in this field.

* Cameras. You can select cameras from the list.

* Date of registration. You can set time limits.

* Search by image. The system supports search by image.

* Accuracy. The field allows you to specify the value of the accuracy of the vehicle attribute, the selection includes values whose determination accuracy is equal to or higher than the entered one.

In the field «Plate number» the license plate is indicated, if any character of the plate is unknown, then this character is replaced by «*». The service supports both Cyrillic and Latin characters. The service supports entering numbers from 7 to 9 characters long.

If you enter a license plate with unknown symbols (for example, A00**A777), then several different numbers may be suitable for this option, some of them are shown in Figure 12.

The «Export to file» button unloads a list of objects that satisfy the filters into a file. This creates a background task; working with tasks is described in section 2.6 of this document.

After entering event filters, click the «Filter» button.

To reset the entered values, press the «Reset» button.

2.1.2. Filter event by Image
""""""""""""""""""""""""""""""""""""""

To search for events with a specific vehicle, it is also possible to upload a vehicle image in the «.jpg» or «.png» format to the system. This can be done using the «Search by image» (Figure 14).

2.1.3. Viewing images by event
""""""""""""""""""""""""""""""""""""""

To view the image of the vehicle or the license plate, click on the corresponding image in the events table (Figure 16). The image will open in a new web browser tab (Figure 17).

2.2. «Incidents» Tab
'''''''''''''''''''''''''''''''''''

An incident is generated if the vehicle attributes in the event coincide with the list record. Lists are created only by the system administrator.

Elements of the «Incidents» interface are shown in Figure 18.

Tab consists of following elements:

* Form for filtering events;

* Table of events;

* Pagination buttons.

The event table contains the following fields:

* Lists – the name of the list on which the incident was registered;

* Time – time of registration;

* Type – vehicle category;

* Brand – vehicle brand;

* Model – vehicle model;

* Emergency service – vehicles belonging to emergency service;

* Color – vehicle color;

* LP country – country of vehicle registration;

* LP number – symbols on vehicles license plate;

* Vehicle photo – thumbnail with a frame of the vehicle, you can open it in a new tab by clicking on photo;

* LP photo – thumbnail with a frame of the license plate, you can open it in a new tab by clicking on photo;

* Track ID – vehicle track number;

* Frame ID – vehicle frame number.

.. note:: The red numbers for each attribute are an assessment of the recognition quality of this attribute.

2.2.1. Incidents Filter
""""""""""""""""""""""""""""""""""""""

Filtering events is possible by one or many attributes at the same time (Figure 19). Some fields support multiple selection.

A description of each field is provided below:

* Lists. Set the lists;

* Plate Number. This field is for entering the number of the license plate;

* Types. You must select the type of vehicle;

* Brands. Field for selecting vehicle brands;

* Models. Field for selecting the vehicle model;

* Colors. This field is used to select the vehicle color;

* Emergency services. Field for selecting emergency services;

* Countries. Countries are selected in this field;

* Cameras. You can select cameras from the list;

* Date of registration. You can set time limits;

* Accuracy. The field allows you to specify the value of the accuracy of the vehicle attribute, the selection includes values whose determination accuracy is equal to or higher than the entered one.

In the field «Plate number» the license plate is indicated, if any character of the plate is unknown, then this character is replaced by «*». The service supports both Cyrillic and Latin characters. The service supports entering numbers from 7 to 9 characters long.

If you enter a license plate with unknown symbols (for example, A00**A777), then several different numbers may be suitable for this option, some of them are shown in Figure 20.

The «Export to file» button unloads a list of objects that satisfy the filters into a file. This creates a background task; working with tasks is described in section 2.6 of this document.

After entering event filters, click the «Filter» button.

To reset the entered values, press the «Reset» button.

2.2.2. Viewing Incident
""""""""""""""""""""""""""""""""""""""

The top menu bar to the right of the section name displays the number of registered unviewed incidents (Figure 22).

2.3. «Lists» Tab
''''''''''''''''''''''''''''''''''

The «Lists» tab displays the lists added by the system administrator. These lists will be used to create incidents.

To start working with lists, you need to go to the «Lists» tab. The view of this section is shown in Figure 26.

The main interface element of this section is a table with lists.

Each list can contain several records.

.. note:: Only the administrator can create, fill, modify and delete lists.

2.3.1. List Creation
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

To create a list, click the «Add» button, and the list settings window will open (Figure 27).

2.3.2. Configurating List Record
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

After the list is saved, the system administrator must configure it. To do this, click on it (Figure 29) and the list configuration window will open (Figure 30).

You must fill in at least 1 attribute or upload a photo.

In the field «License plate» you must enter the vehicle license number, if any symbol of the number is unknown, then this symbol is replaced by «*».

The system supports license plate from 7 to 9 characters long.

If you enter a license plate with unknown character (for example, A00**A777), then several different numbers may be suitable for this option, some of them are shown in Figure 32.

All other fields support multiple choice.

In the «LP Country» field, you can select the country of interest. Since there are numbers of different formats in Russia, you can separately select numbers for special vehicles or «square» format. A complete list of this parameter is presented below:

* Belarus;

* European Union;

* Kazakhstan;

* Russia; 

* Russia (square LP);

* Russia (special transport);

* Ukraine.

In the field «Type» you can select one of 5 categories of vehicles: A, B, C, D, E.

In the fields «Brand» and «Model» you can select the appropriate attributes. If you first select a brand, then in the «Model» field there will be models of this brand only.

Use the «Emergency Services» field to add a search for basic emergency services to the list.

In the «Color» field, you can select one of 16 vehicle colors.

LUNA CARS Analytics supports vehicle search by photo. To achieve the best result, it is necessary to upload photos with a readable number.

The system only supports JPG and PNG images.

You can add an image by dragging it to the download area or click on the download area and select an image in the window that appears.

An example of a record with filled attributes is shown in Figure 33.

To complete the creation of the list, click «Save». If you need to cancel the creation of the list, press [Esc] or click on the area around the window.

2.3.3. Editing and Deleting List Records
""""""""""""""""""""""""""""""""""""""""""""""

If you need to change the list data, you must click the icon to the right of the «Color» column (Figure 34).

This will open the «List entry» window, in which you can change any parameter. The description of each attribute is presented in section 2.3.2 of this document.

After all the changes have been made, you must save them. To cancel changes, press [Esc] or click on the area around the window.

To delete the list, click on the icon next to the edit icon (Figure 35) and confirm the operation (Figure 36).

2.4. «Camera» Tab
''''''''''''''''''''''''''''''''''''''

Like lists, cameras can only be added and edited by administrator. System supports simultaneous work with multiple sources of video streams.

To start working with cameras, you need to go to the «Cameras» tab. The view of this tab is shown in Figure 37.

This tab contains cameras and functions for working with them. The color indicator to the left of the camera name shows the status of the camera, there are 4 statuses:

* Green – camera is working;

* Red – camera error (does not respond to system requests);

* Gray – camera has been disabled by the administrator;

* Blue – camera is trying to connect.

All interaction with cameras takes place using the buttons located on the camera image.

A description of each option will be presented below.

.. note:: The User role does not allow refreshing and editing the camera.

2.4.1. Adding Camera
""""""""""""""""""""""""""""""""""""""

In the right corner of the screen there is a button «Add», you can add a new camera by pressing this button. When you click it, a window for entering new camera data appears (Figure 38).

It is necessary to fill in the attributes of the camera:

* Title. Display name in the camera list. Latin and Cyrillic numbers and letters are allowed.

* CARS Stream Server address. IP-address of CARS Stream where the source will be connected.

* Source Location. Address to RTSP video stream or location of photo / video file.

* Data Transfer Protocol. Video stream transmission protocol. The application can use one of two network protocols to receive video data - TCP or UDP.

* Source type. Video stream or RTSP.

* On/Off toggle for camera operation.

* Angle of rotation (step 90°). The angle of rotation source image. This function used when the incoming video stream is rotated, for example, when the camera is installed on the ceiling.

**TCP** Protocol implements an error control mechanism that minimizes the loss of information and the skip of the reference frames at the cost of increasing the network delay. Key frames are the basis of various compression algorithms used in video codecs (for example, h264). Only the reference frames contain enough information to restore (decode) the image completely, while the intermediate frames contain only differences between adjacent reference frames.

In terms of broadcasting on the network, there is a risk of package loss due to imperfect communication channels. In case of loss of the package containing the data keyframe, the video-stream fragment cannot be correctly decoded. Consequently, distinctive artifacts appear, that are easily and visually distinguishable. These artifacts do not allow the detector to operate in normal mode.

**UDP** protocol does not implement an error control mechanism, so the video-stream is not protected from damage. The use of this protocol is recommended only if there is a high-quality network infrastructure.

It is strongly recommended to use the UPD protocol with a large number of video streams (10 or more).

When all the parameters are configured, you must click the «Save» button.

2.4.2. Editing Detection Zone
""""""""""""""""""""""""""""""""""""""

**ROI (Region of Interest)** specifies the region of interest that will be processed by CARS Stream. ROI can be configured as a rectangle inside the frame.

To configure and edit the ROI, you must click on the «Edit ROI» button.

CARS Stream will process only the zone inside the selected rectangle.

You need to add an image to the camera preview if you want create ROI and DROI. This process is described in clause 3.3.1 of this document.

Correct use of the detection zone significantly improves system performance. The administrator can adjust the detection zone at any time.

An example of detection zone allocation is shown in Figure 39.

For the convenience of setting and changing detection zone, the zone inside the detection zone borders is shaded in yellow (only the selected zone will be processed by CARS Stream). Detection zone can be stretched and squeezed using points. To change the position of the detection zone, click on the yellow zone and drag it inside the preview.

To reset the detection zone setting, press the «Full frame» button.

Geometric parameters of the detection zone are located under the preview:

* Image size. Preview size;

* Coordinates of the detection zone. Coordinates «X» and «Y» with the starting point in the upper left corner of the preview;

* Width and height of the detection zone.

After completing the configuration of the detection zone, click on the «Save» button in the lower left corner of the window.

2.4.3. Adding Recognition Zone
""""""""""""""""""""""""""""""""""""""

DROI (Detection, Region of Interest) sets the recognition zone inside the detection zone. It can be configured as an arbitrary polygon within the frame. The system supports multiple recognition zones. The principle of interaction of objects with the recognition zone is shown in Figure 40.

In this figure, deti is the detection of an object with the number i. Si i is the quantitative value of the intersection of the i-th object and DROI. The Si value will be compared with the value of the threshold, and only when Si is greater than the threshold detection will be considered successful.

.. note:: The threshold is set in the EVENT_CREATION_DROI_THRESHOLD variable, for a description of this variable, see «LUNA CARS Analytics. Administrator's Guide».

Object detection and tracking is performed over the entire detection zone, but the best shot is selected only in the recognition zone. The object detection must be completely inside the recognition zone for the frame to be considered the best. The use of recognition zone allows you to limit the zone to determine the best frame without losing information about the vehicle track outside the recognition zone boundaries.

Changing the position and size of the detection zone does not affect the position and size of the recognition zone.

To configure and edit recognition zone, you must click on the «Add recognition zone»

An example of recognition zone setup is shown in Figure 41.

The algorithm for adding or changing the recognition zone is presented below:

1. To create or edit zone, click the «Start over» button and set the vertices of the polygon, which will be the recognition zone (Figure 41).

2. To complete the construction of the zone, left-click on the starting point of construction of the polygon, or complete the construction of the zone by double-clicking the left mouse button.

3. To save the recognition zone, click the «Save» button located under the preview

4. After that, you must enter the name of this zone in the «Tag Name» field.

5. Optionally, you can save events. To do this, you must activate the "Save events" switch and enter a threshold, upon passing which the event will be saved.

6. To complete the task of the recognition zone, click the «Save» button at the very bottom of the window. To cancel changes, press [Esc] or click on the zone around the window.

If necessary, you can create several recognition zones. The algorithm for creating additional recognition zones completely coincides with the algorithm for adding the first zone. The table with the created recognition zones is in the form of adding a camera (Figure 44). The table consists of several elements:

* Column «Tag», which indicates the name of the zone. Each recognition zone has a unique color, the corresponding tag has the same color;

* Column «Save events», which indicates the threshold for saving the event;

2.4.4. Editing the Recognition Zone
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

2.4.5. Removing the Recognition Zone
""""""""""""""""""""""""""""""""""""""

2.4.6. Restarting the camera
""""""""""""""""""""""""""""""""""""""

In cases when the camera was turned off due to an error (it has a red status indicator), the administrator has the ability to restart the camera in manual mode. The restart button is located on the camera preview (Figure 46).

2.4.7. Editing and Deleting Camera
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

2.4.8. Camera Location
""""""""""""""""""""""""""""""""""""""

To view the location of the camera on the map, click on the map icon (Figure 50).

2.4.9. Video Stream
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

To view the video stream, click on the camera icon (Figure 52).

This opens a window in the browser (Figure 53), where the system detects and tracks the vehicle in the registration zone in real time in the video stream, and on the right, the vehicle's recognized data (track number, model, brand, category) and license plate (image and symbols recognized by the system). Frame boundaries are determined by the settings of the recognition zone (section 2.4.3).

Viewing the processed video is used to debug the camera.

2.5. «User» Tab
''''''''''''''''''''''''''''''''''

The «Users» section is designed to manage user accounts.

When you open «Users» tab, a table with all users of the system appears on the screen (Figure 54).

There are 3 columns in this table:

* Full Name – this field specifies the name of user;

* Login – login is required to log in the system;

* Role – system user role (administrator or user).

A user with the «Administrator» role has access to all sections and allows you to manage user accounts.

Above the table is the filter and search bar. You can find a user by name, login, or filter by roles.

2.5.1. Adding a User
""""""""""""""""""""""""""""""""""""""

In the left corner of the screen there is a button «Add». You can create a new user of the system. When you click it, a window for entering new user data appears (Figure 55).

You must fill in all the fields and select a user role. After saving, the new user will receive a letter to the mail, where he will need to continue registration. If you need to cancel adding a user, you can press [Esc] or click in the area around the window.

2.5.2. Editing and deleting a user. Reset password
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

2.6. «Tasks» Tab
''''''''''''''''''''''''''''''''''''''''

The system implements the ability to upload search results by incidents and images. The request to export to a file is implemented in the system in the form of a task.

The administrator is given the opportunity to view the tasks of all users.

Tasks are created when exporting events and incidents to a file.

You can enter the «Tasks» tab through the drop-down list that appears when you click on the user's photo (Figure 60).

The window consists of a table with a list of background tasks, a filter by author, as well as buttons for selecting a page, selecting the number of elements on a page.

The table contains the following data:

* Author – creator of the task;

* ID – task identifier;

* Type – the type of the task;

* Execution status;

* Creation date – date and time when the task was created;

* Completion date – date and time when the task was completed.

Depending on the result of the execution, the status of the task may change. In total, 4 statuses are applied to tasks in the system:

While the task is being executed, a progress bar is displayed, which allows you to estimate the percentage of the task completion.

The task can be stopped only when it is being executed; in this case, you must click on the «Stop» button (Figure 62).

The stopped task cannot be restarted or continued.

2.6.1. Viewing Task Results
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

To view the results of a task, left-click once on the task row in the task table. The following logic has been implemented:

* Export task – the file will be downloaded;

* Search by image – search results will be displayed;

* The task is incomplete or the result is empty – no action is taken.

The downloaded «.xlsx» file contains a table (Figure 63). The columns of this table are the attributes of the vehicle or the license plate. Additionally, the table displays the location of the images of the vehicle and the license plate.

The search result by image is displayed on a separate page (Figure 64). This page contains a list with all the records found. The list contains the following fields:

* Camera - name of the source that recorded the event;

* Time - time of event registration;

* Type - attribute «vehicle type»;

* Brand - attribute «vehicle brand»;

* Model - attribute «vehicle model»;

* Emergency services - an attribute of the vehicle belonging to emergency services;

* Color - attribute «vehicle color»;

* Country - attribute «vehicle country»;

* Number - attribute «License plate of the vehicle», recognized by the system;

* Vehicle photo - a miniature with a vehicle frame, when pressed, it opens in a new tab;

* Photo of the license plate - a miniature with the license plate of the vehicle, when pressed, it opens in a new tab;

* Track ID - vehicle tracking identifier;

* Frame ID - identifier of the frame with the vehicle image.