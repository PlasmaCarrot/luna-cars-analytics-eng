Release Notes
=====================

1. Versions
-------------------------

1.1. LUNA CARS Analytics 2.0.9
''''''''''''''''''''''''''''''''''''''

* Added multiple DROI.

* Added display of ROI coordinates.

* Added restrictions on stretching ROI.

* Added a button «Full frame», which sets the ROI to the initial position.

* Fixed working with sockets for the first page.

* Fixed the width of the content of the loading indicator cell.

* Removed access to resources for unauthorized users.

* The display in the lists of the Mark-Model pair has been changed.

* Added support for logging events using DROI.

* Added the ability to configure the «media» directory.

* Added role model for each resource.

* Added information on the video stream about the location of the camera.

* Added the ability to export selected fields and images.

* The error of the classifier /cars_api_tester, which did not allow viewing only images of the license plate, has been fixed.

* Fixed interval pagination.

* Fixed export localization.

* Fixed loading images in list entries.

1.2. LUNA CARS Analytics 2.0.8
''''''''''''''''''''''''''''''''''

* Added support of «grz_only_strategy» classifier in /services/frame_process. Also updates on /cars_api_tester interface.

* Fixed camera source type for sync with CARS Stream Server.

* Added small delay before launch background task. 

* Added links to geolocation of the camera on the map.

1.3. LUNA CARS Analytics 2.0.7
''''''''''''''''''''''''''''''''''

* Added endpoint /services/frame_process for frame processing like CARS Stream.

* Added visualization for /services/frame_process in /cars_api_tester.

* Fixed search by images.

1.4. LUNA CARS Analytics 2.0.6
''''''''''''''''''''''''''''''''''

* Added endpoint /auth/reset_password to reset password by email for each user (just send request on email, no block or freeze).

* Added location and «roiPolygon» to the /cams; roi*** fields are now «roiRectangle».

* Added support of «vehicleEmergencyType» for Events, Incidents and PlateListItem.

* Added filtration and visualization of «Emergency Vehicles».

* Fixed translations errors.

* Fixed the lack of a password reset page if the user is already logged in from a PC.

* On a «Lists» tab matching brands and models are now displayed side by side.

* Changed the empty login field to «Email».

1.5. LUNA CARS Analytics 2.0.5
''''''''''''''''''''''''''''''''''

* Added «brand» list item.

* Added additional translation files.

* Added support for UAE countries view.

* Added host:port/cars_api_tester endpoint - the UI resource for sending requests to CARS API service in interactive way.

* Added English language support.

* Added a drop-down list with the addresses of CARS Stream servers.

* Added automatic loading of photos on image search.

* Added cameras on the lists.

* Added processing for large images.

* Fixed value sent when selecting the camera type «Image folder».

* Button for creating task is renamed.

* Number of pages to select pagination is changed.

* Changed access rights in the role model.

1.6. LUNA CARS Analytics 2.0.4
''''''''''''''''''''''''''''''''''

* Changed the names of the results files of export tasks to a file in «.xlsx» format.

* At least one field must be filled in to create or modify a list.

* Added administration manual.

* Added user manual.

1.7. LUNA CARS Analytics 2.0.3
''''''''''''''''''''''''''''''''''

* Added the ability to deploy via Docker.

* Added functional tests.

* Fixed critical bugs.

* Fixed deployment issues

1.8. LUNA CARS Analytics 2.0.2
''''''''''''''''''''''''''''''''''

* Implemented management of CARS Stream Server streams via «Cameras» endpoint.

* Added Admin page via Django Admin framework.

1.9. LUNA CARS Analytics 2.0.1
''''''''''''''''''''''''''''''''''

* Replacing the old CARS Analytics interface with a separate application.

* Implementation of basic functionality.

* Backend is implemented as an independent REST API module on Django 3.1.

* Added user authorization in the system, to access the new analytics, you need an account.

* Added user and administrator access rights.

* Added the «Lists» and «List items» (filter presets) for creating incidents based on events.

* Added the «Incident» model.

* When new events and incidents occur in the system, objects on the web browser page are updated without the need to refresh the page (2 web socket channels are implemented on Django-Channels).

* Extended filtering of objects in the «Events» and «Incidents» endpoints.

* Search for events by image and export is implemented through background tasks (Celery framework).

* Added task management (filter, view results, stop and delete).